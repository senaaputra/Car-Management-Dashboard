# BINARENTAL CAR

This project is a simple project for implementing crud rest api using express js, mysql, and sequelize

link repository carRental-Backend

```
https://gitlab.com/senaaputra/carRental-Backend
```

# File Structure

<pre>
📦Car-Management-Dashboard
┣ 📂node_modules
┣ 📂public
┃ ┣ 📂image
┃ ┃ ┗ 📜script.js
┃ ┣ 📜script.js
┃ ┣ 📜style.js
┃ ┣📂views
┃ ┃ ┣ 📂components
┃ ┃ ┃ ┣ 📜head.ejs
┃ ┃ ┃ ┣ 📜navbar.ejs
┃ ┃ ┃ ┣ 📜script.ejs
┃ ┃ ┃ ┗ 📜sidebar.ejs
┃ ┃ ┣ 📂layouts
┃ ┃ ┃ ┗ 📜index.ejs
┃ ┃ ┣ 📂pages
┃ ┃ ┃ ┣ 📜404.ejs
┃ ┃ ┃ ┣ 📜cars.ejs
┃ ┃ ┃ ┣ 📜edit.ejs
┃ ┃ ┃ ┗ 📜add.ejs
┣ 📜.gitignore
┣ 📜index.js
┣ 📜package-lock.json
┣ 📜package.json
┗ 📜README.md
</pre>

# Requirements

- Code Editor ( VSCode )

```vscode
https://code.visualstudio.com/
```

- Bootstrap V-5

```bootstrap
https://getbootstrap.com/
```

- Node js

```nodejs
https://nodejs.org/en/download/
```

- Express js

```expressjs
https://expressjs.com/
```

- MySQL

```mysql
https://www.mysql.com/
```

- Sequelize

```sequelize
https://sequelize.org/
```

- Nodemon

```nodemon
https://www.npmjs.com/package/nodemon
```

# Design DB

<img src="public/image/designDB.jpeg" width="400"/>

# Instalating Guide

## Download repository files

1. Download repository files

- Download directly
  Download the repository file in zip
- Or clone from this repository

```bash
https://gitlab.com/senaaputra/Car-Management-Dashboard.git
```

2. Place it according to the disired repository

3. open it in VSCode or other code editors

4. install node_module in terminal (npm install --save)

5. create database car_rental in MySQL

6. running project in terminal (npm start)
