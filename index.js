// creating server with express and also using ejs as view engine
const express = require("express");
const axios = require("axios");
const bodyParser = require("body-parser");
const expressLayouts = require("express-ejs-layouts");

const app = express();
const PORT = process.env.PORT || 8000;

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  const { filter, all, small, medium, large } = req.query;
  axios.get("http://localhost:8001/api/cars/allCars").then((cars) => {
    let dataTampil = cars.data;
    if (filter) {
      dataTampil = cars.data.filter((car) => {
        const filterer = car.car_name;
        const filtering = filterer.match(filter);
        return filtering;
      });
    }
    if (all) {
      dataTampil = cars.data.filter((car) => {
        const filterer = car.car_size.toLowerCase();
        const filtering = filterer.match(all.toLowerCase());
        return filtering;
      });
    }
    if (small) {
      dataTampil = cars.data.filter((car) => {
        const filterer = car.car_size.toLowerCase();
        const filtering = filterer.match(small.toLowerCase());
        return filtering;
      });
    }
    if (medium) {
      dataTampil = cars.data.filter((car) => {
        const filterer = car.car_size.toLowerCase();
        const filtering = filterer.match(medium.toLowerCase());
        return filtering;
      });
    }
    if (large) {
      dataTampil = cars.data.filter((car) => {
        const filterer = car.car_size.toLowerCase();
        const filtering = filterer.match(large.toLowerCase());
        return filtering;
      });
    }

    res.render("pages/cars", {
      item: dataTampil,
      filter,
      layout: "layouts/index",
      page: "Cars",
      page1: "List Car",
      page2: "Cars",
    });
  });
});
app.get("/add", (req, res) => {
  axios.put(`http://localhost:8001/api/cars/addCar`).then((cars) => {
    res.render("pages/edit", {
      layout: "layouts/index",
      page: "Cars",
      page1: "List Car",
      page2: "Add New Car",
    });
  });
});
// app.get("/edit", (req, res) => {
//   res.render("pages/add", {
//     layout: "layouts/index",
//     page: "Cars",
//     page1: "List Car",
//     page2: "Update Car Information",
//   });
// });

app.get("/edit", (req, res) => {
  const id = parseInt(req.params.id);
  axios.put(`http://localhost:8001/api/cars/${id}`).then((cars) => {
    res.render("pages/edit", {
      layout: "layouts/index",
      dataPilih: cars.data,
      page: "Cars",
      page1: "Car List",
      page2: "Edit Car",
    });
  });
});

// app.use((req, res) => {
//   res.status(404).render("pages/404", {
//     layout: "layouts/index",
//     page: "404",
//   });
// });

app.listen(PORT, () => {
  console.log(`Server berjalan di http://localhost:${PORT}`);
});
